# setInterval clock

Create a button that when clicked turns green.  2 seconds after the button was clicked the button should turn back to black.

**Note:** Styles are provided for you in master.css. Look in the file to find the approriate class names to use on your HTML elements.

### Example

[The example is here](https://settimeout-colourtimer.now.sh).

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Add a button into the HTML, make sure to add the bigbutton class
2. Select the button in JavaScript
3. Add the event listener onto the button
4. Create a callback function that is passed into the event listener.  The function should add the clickedbutton class onto the button elements class list
1. Insdie the click callback function, call setTimeout with another callback.  Give setTimeout a 2 second delay (2000 milliseconds).
1. Inside the setTimeout callback, remove the clickedbutton class from the button element.